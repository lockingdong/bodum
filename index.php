
<?php include("./env.php"); ?>
<?php

	
	$pd_id = $_GET['pd_id'];
	$og_id = $_GET['og_id'];
	$host = $_SERVER['HTTP_HOST'];
	$uri = $_SERVER['REQUEST_URI'];
	$url = 'http://' . $host . $uri;
	$url = preg_replace('/\?.*/', '', $url);
	//echo $url;
	

	

	switch ($pd_id) {
		case 'pd00':
			// $f_url = ;
			// echo $f_url;
			header("Location:". $url . '?og_id=pd00#/products/0' );
			$og_title = "儲物罐600cc";
			$og_image = "http://test.moveon-design.com/pximages/00.png";
			$og_des = "寬口徑設計的儲存罐，不論是零食或是咖啡粉皆可輕鬆儲存。罐身使用可回收材質搭配矽膠防滑圈，設計簡約中帶點時尚，屬於Bodum的經典北歐風格。";
			break;
		case 'pd01':
			header("Location:". $url . '?og_id=pd01#/products/1' );
			$og_title = "不鏽鋼真空隨行杯0.35L";
			$og_image = "http://test.moveon-design.com/pximages/011.png";
			$og_des = "真空的不鏽鋼設計，保冷又可保溫。杯身配以符合人體工學的矽膠圈，好握、好拿方便帶著走，防滑橡膠底座使您放置時不易翻倒。";
			break;
		case 'pd02':
			header("Location:". $url . '?og_id=pd02#/products/2' );
			$og_title = "雙層玻璃杯250cc（2入/組)";
			$og_image = "http://test.moveon-design.com/pximages/02.png";
			$og_des = "專業工匠手工吹造、杯底專利矽膠氣孔，堪稱「會呼吸」的雙層玻璃杯。雙層設計可高效隔絕空氣，使外層玻璃壁不易產生水珠，透明無色的玻璃杯不論是盛裝飲品或是擺放點心都合適。";
			break;
		case 'pd03':
			header("Location:". $url . '?og_id=pd03#/products/3' );
			$og_title = "哥本哈根保溫瓶1.1L";
			$og_image = "http://test.moveon-design.com/pximages/03.png";
			$og_des = "專業工匠手工吹造、杯底專利矽膠氣孔，堪稱「會呼吸」的雙層玻璃杯。雙層設計可高效隔絕空氣，使外層玻璃壁不易產生水珠，透明無色的玻璃杯不論是盛裝飲品或是擺放點心都合適。";
			break;
		case 'pd04':
			header("Location:". $url . '?og_id=pd04#/products/4' );
			$og_title = "美式濾滴咖啡機";
			$og_image = "http://test.moveon-design.com/pximages/04.png";
			$og_des = "擁有美式咖啡的低調質感，也同時具有Bodum對於咖啡的堅持，咖啡機獨特的噴頭設計，使熱水更均勻的撒在咖啡粉上，媲美職人手沖口感。此外，配有金屬濾網可直接使用免濾紙、預約定時功能簡單設定即可品嘗咖啡香醇風味。";
			break;
		case 'pd05':
			header("Location:". $url . '?og_id=pd05#/products/5' );
			$og_title = "鬆餅機";
			$og_image = "http://test.moveon-design.com/pximages/05.png";
			$og_des = "五段溫度設定，不僅可烤出完美鬆餅，也可依喜好製作馬鈴薯餅、起士餅及蛋餅等。不易沾黏的烤盤方便清潔，機身底部配有隱藏式電源線收納，外型時尚宛如餐桌上的家飾品。";
			break;
		case 'pd06':
			header("Location:". $url . '?og_id=pd06#/products/6' );
			$og_title = "多段式磨豆機";
			$og_image = "http://test.moveon-design.com/pximages/06.png";
			$og_des = "專業級錐形研磨刀盤搭載12段模式溫柔均勻研磨，滿足您從義式咖啡至法式濾壓壺的咖啡研磨需求！專業級的磨豆機是身為咖啡愛好者的你，絕對不可缺少的好幫手。";
			break;
		case 'pd07':
			header("Location:". $url . '?og_id=pd07#/products/7' );
			$og_title = "電動虹吸式咖啡壺";
			$og_image = "http://test.moveon-design.com/pximages/07.png";
			$og_des = "專業級虹吸式咖啡也可以很簡單，採用電熱式加熱，使咖啡製作時間和溫度完美校準、確保每杯咖啡的風味，魔術般的沖煮過程讓人充分感受動手煮咖啡的樂趣。";
			break;
	}

	switch ($og_id) {
		case 'pd00':
			$og_title = "儲物罐600cc";
			$og_image = "http://test.moveon-design.com/pximages/00.png";
			$og_des = "寬口徑設計的儲存罐，不論是零食或是咖啡粉皆可輕鬆儲存。罐身使用可回收材質搭配矽膠防滑圈，設計簡約中帶點時尚，屬於Bodum的經典北歐風格。";
			break;
		case 'pd01':
			
			$og_title = "不鏽鋼真空隨行杯0.35L";
			$og_image = "http://test.moveon-design.com/pximages/011.png";
			$og_des = "真空的不鏽鋼設計，保冷又可保溫。杯身配以符合人體工學的矽膠圈，好握、好拿方便帶著走，防滑橡膠底座使您放置時不易翻倒。";
			break;
		case 'pd02':
			
			$og_title = "雙層玻璃杯250cc（2入/組)";
			$og_image = "http://test.moveon-design.com/pximages/02.png";
			$og_des = "專業工匠手工吹造、杯底專利矽膠氣孔，堪稱「會呼吸」的雙層玻璃杯。雙層設計可高效隔絕空氣，使外層玻璃壁不易產生水珠，透明無色的玻璃杯不論是盛裝飲品或是擺放點心都合適。";
			break;
		case 'pd03':
			
			$og_title = "哥本哈根保溫瓶1.1L";
			$og_image = "http://test.moveon-design.com/pximages/03.png";
			$og_des = "專業工匠手工吹造、杯底專利矽膠氣孔，堪稱「會呼吸」的雙層玻璃杯。雙層設計可高效隔絕空氣，使外層玻璃壁不易產生水珠，透明無色的玻璃杯不論是盛裝飲品或是擺放點心都合適。";
			break;
		case 'pd04':
			
			$og_title = "美式濾滴咖啡機";
			$og_image = "http://test.moveon-design.com/pximages/04.png";
			$og_des = "擁有美式咖啡的低調質感，也同時具有Bodum對於咖啡的堅持，咖啡機獨特的噴頭設計，使熱水更均勻的撒在咖啡粉上，媲美職人手沖口感。此外，配有金屬濾網可直接使用免濾紙、預約定時功能簡單設定即可品嘗咖啡香醇風味。";
			break;
		case 'pd05':
			
			$og_title = "鬆餅機";
			$og_image = "http://test.moveon-design.com/pximages/05.png";
			$og_des = "五段溫度設定，不僅可烤出完美鬆餅，也可依喜好製作馬鈴薯餅、起士餅及蛋餅等。不易沾黏的烤盤方便清潔，機身底部配有隱藏式電源線收納，外型時尚宛如餐桌上的家飾品。";
			break;
		case 'pd06':
			
			$og_title = "多段式磨豆機";
			$og_image = "http://test.moveon-design.com/pximages/06.png";
			$og_des = "專業級錐形研磨刀盤搭載12段模式溫柔均勻研磨，滿足您從義式咖啡至法式濾壓壺的咖啡研磨需求！專業級的磨豆機是身為咖啡愛好者的你，絕對不可缺少的好幫手。";
			break;
		case 'pd07':
			
			$og_title = "電動虹吸式咖啡壺";
			$og_image = "http://test.moveon-design.com/pximages/07.png";
			$og_des = "專業級虹吸式咖啡也可以很簡單，採用電熱式加熱，使咖啡製作時間和溫度完美校準、確保每杯咖啡的風味，魔術般的沖煮過程讓人充分感受動手煮咖啡的樂趣。";
			break;
	}


	$pd_info = '{
		"title": "'.$og_title.'",
		"image": "'.$og_image.'",
		"des": "'.$og_des.'"

	}';
	$obj = json_decode($pd_info);
?>
<!DOCTYPE html>
<html lang="zh-Hant-TW">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta property="og:image" content="<?php echo ($obj->{'image'})?$obj->{'image'}:'http://test.moveon-design.com/pximages/index-img.png'?>">
		<meta property="og:title" content="<?php echo ($obj->{'title'})?$obj->{'title'}:'bodum 北歐時尚 品味生活'?>">
		<meta property="og:description" content="<?php echo ($obj->{'des'})?$obj->{'des'}:'1944年創立於丹麥的哥本哈根，產品開發以實用為本。北歐經典設計更多次獲得國際大獎，在咖啡領域享譽世界！我們深信，幸福發生在每個平凡日常，從Bodum開始，讓「在家喝咖啡」，變成您日常最幸福的事！'?>"/>
		<title><?php echo $obj->{'title'}; ?></title>
		<link rel="icon" type="image/png" href="src/assets/paxmart.png">
		<link rel="stylesheet prefetch" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">
		<link rel="stylesheet prefetch" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css">
		<link rel="stylesheet" href="https://jongacnik.github.io/slick-lightbox/dist/slick-lightbox.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
		<style>
	
		</style>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<!-- 東 -->
		<?php if($dongga): ?>
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125507822-3"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-125507822-3');
		</script>
		<?php endif; ?>


		<!-- Global site tag (gtag.js) - Google Analytics -->
		<!-- 喬 -->
		<?php if($joga): ?>
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118982674-4"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-118982674-4');
		</script>
		<?php endif; ?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<!-- 全聯 -->
		<!-- Global site tag (gtag.js) - Google Analytics -->


		<?php if($pxga): ?>
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-55952023-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		 
		  gtag('config', 'UA-55952023-1');
		</script>
		<?php endif; ?>






		<!-- Google Tag Manager -->
		<!-- 全聯 -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-5WKG5B2');</script>
		<!-- End Google Tag Manager -->
	</head>
	<body><div id="app"></div>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5WKG5B2"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<script type="text/javascript" class="amnet_dmp_tag">
			var amnet_i_src = 'pt.amnetgroup.com.tw/acjs/cyd_pxmart.js';
			var amnet_i_ct = document.createElement('script');
			var src = (location.protocol == 'https:') ? 'https://' : 'http://';
			amnet_i_ct.setAttribute('src',src+amnet_i_src); amnet_i_ct.charset = 'utf-8';
			var amnet_i_cts = document.getElementsByTagName('script')[0];
			amnet_i_cts.parentNode.insertBefore(amnet_i_ct, amnet_i_cts);
		</script>

		<script>history.scrollRestoration = "manual"</script>
		<script>
			
			var cf_link = <?php echo "'" . $cf_link . "'"; ?>;
			if (!window['YT']) {var YT = {loading: 0,loaded: 0};}if (!window['YTConfig']) {var YTConfig = {'host': 'http://www.youtube.com'};}if (!YT.loading) {YT.loading = 1;(function(){var l = [];YT.ready = function(f) {if (YT.loaded) {f();} else {l.push(f);}};window.onYTReady = function() {YT.loaded = 1;for (var i = 0; i < l.length; i++) {try {l[i]();} catch (e) {}}};YT.setConfig = function(c) {for (var k in c) {if (c.hasOwnProperty(k)) {YTConfig[k] = c[k];}}};var a = document.createElement('script');a.type = 'text/javascript';a.id = 'www-widgetapi-script';a.src = 'https://s.ytimg.com/yts/jsbin/www-widgetapi-vflxGrywa/www-widgetapi.js';a.async = true;var c = document.currentScript;if (c) {var n = c.nonce || c.getAttribute('nonce');if (n) {a.setAttribute('nonce', n);}}var b = document.getElementsByTagName('script')[0];b.parentNode.insertBefore(a, b);})();}
		</script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
		<!-- <script src="/bodum/dist/slick.js"></script> -->
		<!-- <script src="//www.youtube.com/player_api"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="/bodum/dist/slick.js"></script>
		<script src="/bodum/dist/slick_lightbox.js"></script>
		<script src="/bodum/dist/build81.js"></script>
		
	</body>
	
</html>
