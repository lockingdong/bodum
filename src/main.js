import Vue from 'vue';
import router from './router';
import ToTopBtn from './ToTopBtn';
import eventBus from './eventBus';

Vue.component('ToTopBtn', ToTopBtn);

new Vue({
	el: '#app',
	//render: h => h(App)
	data() {
        return {
            // Bind our event bus to our $root Vue model
            bus: eventBus
        }
    },
	router,
    template: '<router-view/>',
    mounted(){
        console.log("app.vue!!!");

    }
})
